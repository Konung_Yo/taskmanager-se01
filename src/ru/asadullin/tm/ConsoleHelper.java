package ru.asadullin.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * utility class help with console
 */
public class ConsoleHelper {
    private Model model;
    private static BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));

    public ConsoleHelper(Model model) {
        this.model = model;
    }

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public static String readString() {
        String mes = null;
        try {
            mes = bis.readLine();
        } catch (IOException e) {
            System.out.println("Произошла ошибка при попытке ввода текста. Попробуйте еще раз.");
            mes = readString();
        }
        return mes;
    }

    public static int readInt() {
        Integer res = null;
        try {
            res = Integer.parseInt(readString().trim());
        } catch (NumberFormatException e) {
            System.out.println("Произошла ошибка при попытке ввода цифры. Попробуйте еще раз.");
            res = Integer.parseInt(readString().trim());
        }
        return res;
    }

    public void selectCommand(String command) {
        String[] selectCommand = command.split(" ");
        if (selectCommand[0].equals("project-select")) {
            StringBuilder projectName = new StringBuilder(selectCommand[1]);
            for (int i = 2; i < selectCommand.length; i++) {
                projectName.append(" ").append(selectCommand[i]);
            }
            model.selectProject(projectName.toString());
        } else if (selectCommand[0].equals("task-select")) {
            StringBuilder taskName = new StringBuilder(selectCommand[1]);
            for (int i = 2; i < selectCommand.length; i++) {
                taskName.append(" ").append(selectCommand[i]);
            }
            model.selectTask(taskName.toString());
        } else {
            writeMessage("Wrong command, please type \"help\" for a list of commands.");
        }
    }

    public static boolean confirmOperation(String type, String name) {
        writeMessage("Confirm removal a " + type + " " + name + "? y/n");
        while (true) {
            String answer = readString();
            if (answer.equalsIgnoreCase("y")) return true;
            else if (answer.equalsIgnoreCase("n")) return false;
            else writeMessage("Wrong command, please type \"y\" for Yes, or \"n\" for No");
        }
    }

    public static void helpCommand() {
        System.out.println("help: Show all commands.\n" +
                "project-select \"name\": Select project by name.\n" +
                "project-edit: Edit selected project.\n" +
                "project-clear: Remove all projects.\n" +
                "project-create: Create new project.\n" +
                "project-list: Show all projects.\n" +
                "project-delete:Remove selected project.\n" +
                "task-select \"name\": Select project by name.\n" +
                "task-edit: Edit selected project.\n" +
                "task-clear: Remove all tasks.\n" +
                "task-create: Create new task.\n" +
                "task-list: Show all tasks.\n" +
                "task-delete: Remove selected task.\n" +
                "exit: Exit the Task manager");
    }
}
