package ru.asadullin.tm;

public class TaskManager {
    public static void main(String[] args) {
        ConsoleHelper.writeMessage("*** WELCOME TO TASK MANAGER ***");
        final Model model = new Model();
        ConsoleHelper consoleHelper = new ConsoleHelper(model);
        while (true) {
            String command = ConsoleHelper.readString().trim();
            if (command.split(" ").length >= 2) {
                consoleHelper.selectCommand(command);
            } else {
                if (command.equalsIgnoreCase("exit")) {
                    break;
                }
                switch (command.toLowerCase()) {
                    case "help":
                        ConsoleHelper.helpCommand();
                        break;
                    case "project-edit":
                        ConsoleHelper.writeMessage(" [EDIT PROJECT]");
                        model.editProject();
                        break;
                    case "project-clear":
                        ConsoleHelper.writeMessage(" [CLEAR PROJECTS]");
                        model.clearProjects();
                        break;
                    case "project-create":
                        ConsoleHelper.writeMessage(" [PROJECT CREATE]");
                        model.addNewProject();
                        ConsoleHelper.writeMessage("");
                        break;
                    case "project-list":
                        ConsoleHelper.writeMessage(" [PROJECT LIST]");
                        model.showProjectsList();
                        ConsoleHelper.writeMessage("");
                        break;
                    case "project-delete":
                        ConsoleHelper.writeMessage(" [PROJECT DELETE]");
                        model.removeProjectFromList();
                        ConsoleHelper.writeMessage("");
                        break;
                    case "task-edit":
                        ConsoleHelper.writeMessage(" [EDIT TASK]");
                        model.editTask();
                        break;
                    case "task-clear":
                        ConsoleHelper.writeMessage(" [CLEAR TASKS]");
                        model.clearTasks();
                        break;
                    case "task-create":
                        ConsoleHelper.writeMessage(" [TASK CREATE]");
                        model.addNewTask();
                        ConsoleHelper.writeMessage("");
                        break;
                    case "task-list":
                        ConsoleHelper.writeMessage(" [TASK LIST]");
                        model.showTasksList();
                        ConsoleHelper.writeMessage("");
                        break;
                    case "task-delete":
                        ConsoleHelper.writeMessage(" [DELETE TASK]");
                        model.removeTaskFromList();
                        ConsoleHelper.writeMessage("");
                        break;
                    default:
                        ConsoleHelper.writeMessage("Wrong command, please type \"help\" for a list of commands.");
                        break;
                }
            }
        }
    }
}
