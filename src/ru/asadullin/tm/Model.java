package ru.asadullin.tm;

import ru.asadullin.tm.objects.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Model {

    private List<Project> projects;
    private List<Task> tasks;
    private Project selectedProject;
    private Task selectedTask;

    public Model() {
        projects = new ArrayList<>();
    }

    public void clearProjects() {
        projects.clear();
        ConsoleHelper.writeMessage(" [ALL PROJECTS REMOVED]");
    }

    public void addNewProject() {
        ConsoleHelper.writeMessage("ENTER NAME:");
        String name = ConsoleHelper.readString();
        ConsoleHelper.writeMessage("ENTER DESCRIPTION:");
        String description = ConsoleHelper.readString();
        projects.add(new Project(name, description));
        ConsoleHelper.writeMessage(" [OK]");
        ConsoleHelper.writeMessage("");
    }

    public void showProjectsList() {
        if (projects.isEmpty()) return;
        projects.sort(Comparator.comparing(Project::getName));
        for (int i = 0; i < projects.size(); i++) {
            ConsoleHelper.writeMessage((i + 1) + ". " + projects.get(i).getName());
        }
    }

    public void removeProjectFromList() {
        showProjectsList();
        ConsoleHelper.writeMessage("ENTER PROJECT TO REMOVE:");
        String name = ConsoleHelper.readString();
        boolean isFound = false;
        for (Project project : projects) {
            if (project.getName().equals(name)) {
                isFound = true;
                if (ConsoleHelper.confirmOperation("project", project.getName())) {
                    projects.remove(project);
                    ConsoleHelper.writeMessage(" [OK]");
                    break;
                }
            }
        }
        if (!isFound) {
            ConsoleHelper.writeMessage("NO SUCH PROJECT EXISTS");
        }
    }

    public void selectProject(String projectName) {
        boolean isSelected = false;
        for (Project project : projects) {
            if (project.getName().equals(projectName)) {
                selectedProject = project;
                isSelected = true;
                break;
            }
        }
        if (isSelected) {
            ConsoleHelper.writeMessage("Name of project: " + selectedProject.getName());
            ConsoleHelper.writeMessage("Description of project: " + selectedProject.getDescription());
        } else {
            ConsoleHelper.writeMessage("NO SUCH PROJECT EXISTS");
        }
    }


    public void editProject() {
        if (selectedProject != null) {
            ConsoleHelper.writeMessage("ENTER NAME:");
            String name = ConsoleHelper.readString();
            selectedProject.setName(name);
            ConsoleHelper.writeMessage("ENTER DESCRIPTION:");
            String description = ConsoleHelper.readString();
            selectedProject.setDescription(description);
        } else {
            ConsoleHelper.writeMessage("THERE IS NO SELECTED PROJECT, PLEASE WRITE NAME OF PROJECT TO EDIT");
            selectProject(ConsoleHelper.readString());
            editProject();
        }
    }

    public void clearTasks() {
        selectProject();
        selectedProject.getTasks().clear();
        ConsoleHelper.writeMessage(" [ALL TASKS REMOVED]");
    }

    public void addNewTask() {
        if (projects.isEmpty()) {
            ConsoleHelper.writeMessage("THERE IS NO PROJECTS TO ADD NEW TASK");
            return;
        }
        selectProject();
        ConsoleHelper.writeMessage("ENTER NAME:");
        String name = ConsoleHelper.readString();
        ConsoleHelper.writeMessage("ENTER DESCRIPTION:");
        String description = ConsoleHelper.readString();
        selectedProject.getTasks().add(new Task(name, description, selectedProject.getName()));
        ConsoleHelper.writeMessage(" [OK]");
    }

    public void showTasksList() {
        selectProject();
        if (selectedProject == null) {
            return;
        }
        List<Task> selectedProjectTasks = selectedProject.getTasks();
        if (selectedProjectTasks.isEmpty()) return;
        selectedProjectTasks.sort(Comparator.comparing(Task::getName));
        for (int i = 0; i < selectedProjectTasks.size(); i++) {
            ConsoleHelper.writeMessage((i + 1) + ". " + selectedProjectTasks.get(i));
        }
    }

    public void removeTaskFromList() {
        selectProject();
        if ((selectedProject == null) || (selectedProject.getTasks().isEmpty())) {
            ConsoleHelper.writeMessage("NO SUCH TASKS TO REMOVE");
            return;
        }
        for (int i = 0; i < selectedProject.getTasks().size(); i++) {
            ConsoleHelper.writeMessage((i + 1) + ". " + selectedProject.getTasks().get(i));
        }
        while (true) {
            ConsoleHelper.writeMessage("ENTER TASK TO REMOVE:");
            String name = ConsoleHelper.readString();
            boolean isRemoved = false;
            for (Task task : selectedProject.getTasks()) {
                if (task.getName().equalsIgnoreCase(name)) {
                    selectedProject.getTasks().remove(task);
                    isRemoved = true;
                    break;
                }
            }
            if (isRemoved) {
                ConsoleHelper.writeMessage(" [OK]");
                break;
            } else {
                ConsoleHelper.writeMessage("NO SUCH TASK EXISTS");
            }
        }

    }

    public void selectTask(String taskName) {
        boolean isSelected = false;
        for (Project project : projects) {
            for (Task task : project.getTasks()) {
                if (task.getName().equals(taskName)) {
                    selectedTask = task;
                    isSelected = true;
                    break;
                }
            }
        }
        if (isSelected) {
            ConsoleHelper.writeMessage("Name of task: " + selectedTask.getName());
            ConsoleHelper.writeMessage("Project: " + selectedTask.getProject());
            ConsoleHelper.writeMessage("Description of task: " + selectedTask.getDescription());
        } else {
            ConsoleHelper.writeMessage("NO SUCH TASK EXISTS");
        }
    }

    public void editTask() {
        if (selectedTask != null) {
            ConsoleHelper.writeMessage("ENTER NAME:");
            String name = ConsoleHelper.readString();
            selectedTask.setName(name);
            ConsoleHelper.writeMessage("ENTER DESCRIPTION:");
            String description = ConsoleHelper.readString();
            selectedTask.setDescription(description);
            ConsoleHelper.writeMessage(" [OK]");
        } else {
            ConsoleHelper.writeMessage("THERE IS NO SELECTED TASK, PLEASE WRITE NAME OF TASK TO EDIT");
            showTasksList();
            if (projects.isEmpty()) {
                return;
            }
            selectTask(ConsoleHelper.readString());
            editTask();
        }
    }

    private void selectProject() {
        showProjectsList();
        ConsoleHelper.writeMessage("ENTER NAME OF PROJECT:");
        String name = ConsoleHelper.readString();
        boolean isSelected = false;
        for (Project project : projects) {
            if (project.getName().equals(name)) {
                selectedProject = project;
                isSelected = true;
                break;
            }
        }
        if (isSelected) {
            ConsoleHelper.writeMessage("SELECTED PROJECT \"" + selectedProject.getName() + "\"");
        } else {
            ConsoleHelper.writeMessage("NO SUCH PROJECT EXISTS");
        }
    }
}