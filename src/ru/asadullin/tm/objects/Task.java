package ru.asadullin.tm.objects;

import java.time.LocalDateTime;

public class Task {
    private String name;
    private String description;
    // TODO: 13.12.2022 string to project
    private String project;
    private LocalDateTime createDate;

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Task(String name, String description, String project) {
        this.name = name;
        this.project = project;
        this.description = description;
        this.createDate = LocalDateTime.now();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getProject() {
        return project;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @Override
    public String toString() {
        return name;
    }

}
