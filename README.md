# Task Manager

### Описание проекта
TASK MANAGER - программа, в которой можно создавать свои проекты и задачи. В каждый проект можно добавлять описание, а так же его редактировать. Каждую задачу можно создавать в общем списке или внутри созданного проекта, так же добавлять задачу в проект. При просмотре проекта или задачи - будет выведено название, описание, время и дата создания.

### Требования к SOFTWARE
```
MacOs
Windows
Linux
```

### Стек технологий
```
IntelliJ IDEA Ultimate
Java SE Development 8
Git
```

### Разработчик
```
Асадуллин Эльнур Маратович
Asadelmar@gmail.com
```

### Консольные команды:
```
help: Show all commands.
project-clear: Remove all projects.
project-create: Create new project.
project-list: Show all projects.
project-remove:Remove selected project.
task-clear: Remove all tasks.
task-create: Create new task.
task-list: Show all tasks.
task-remove: Remove selected task.
exit: Exit the Task manager
```
